import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import { useHistory, useLocation } from 'react-router-dom'
import Layout from '../shared/Layout';
import './css/home.css'
import GameList from '../shared/GameList';
import Loading from '../shared/Loading';

const Home = (props) => {

    const history = useHistory();

    const [gameData, setGameData] = useState(null);

    const [formWidth, setFormWidth] = useState(0);

    const [scrollLength, setScrollLength] = useState(0);

    const [input, setInput] = useState('');

    const [page, setPage] = useState(1);

    const [itemsCount, setItemsCount] = useState(0);
    
    const formRef = useRef(0);

    
    useEffect(() => {

        getGamesData();
        
        return () => {
            
        }
    }, [])

    useEffect(() => {
        if(formRef.current) setFormWidth(formRef.current.offsetWidth);
    }, [])


    const resizeDiv = () => {
        if(formRef.current) setFormWidth(formRef.current.offsetWidth);
    }

    window.addEventListener('resize', resizeDiv);

    useEffect(() => {
        const handleScroll = () => {
            
            setScrollLength(window.scrollY);

        };

        window.addEventListener("scroll", handleScroll, { passive: true });
        

        return () => {
            window.removeEventListener("scroll", handleScroll);
            window.removeEventListener('resize', resizeDiv);
        };
    }, [scrollLength]);

    const getGamesData = async () => {

        const searchParams = new URLSearchParams(props.location.search);

        const search = searchParams.get("search");
        const pageQuery = searchParams.get("page");

        const res = await axios.get('https://api.rawg.io/api/games', {
            params: {
                key: process.env.REACT_APP_API_KEY,
                search: search ? search : '',
                page: pageQuery ? pageQuery : 1,
                page_size: 10
            }
        })

        const gamesResponse = res.data.results ? res.data.results : null;
        const itemsC = res.data.count ? res.data.count : 0;

        setGameData(gamesResponse);
        setItemsCount(itemsC);
        setPage(parseInt(pageQuery)?parseInt(pageQuery):1);
    }

    const searchGame = async() => {

        history.push({
            pathname:'/',
            search:`?search=${input}&page=${1}`
        })
        
        const res = await axios.get('https://api.rawg.io/api/games', {
            params: {
                key: process.env.REACT_APP_API_KEY,
                search: input,
                page: 1,
                page_size: 10
            }
        })

        const gamesResponse = res.data.results ? res.data.results : null;
        const itemsC = res.data.count ? res.data.count : 0;

        setGameData(gamesResponse);
        setItemsCount(itemsC);
        setPage(1);
    }

    const changePage = async(next) => {

        const searchParams = new URLSearchParams(props.location.search);

        const search = searchParams.get("search") ? searchParams.get("search") : '';

        if(next) {
            
            history.push({
                pathname:'/',
                search:`?search=${search}&page=${page+1}`,
            })
            
        } else {
            history.push({
                pathname:'/',
                search:`?search=${search}&page=${page-1}`,
            })
        }

        const res = await axios.get('https://api.rawg.io/api/games', {
            params: {
                key: process.env.REACT_APP_API_KEY,
                search: search,
                page: next ? page+1 : page-1,
                page_size: 10
            }
        })

        const gamesResponse = res.data.results ? res.data.results : null;
        const itemsC = res.data.count ? res.data.count : 0;

        setGameData(gamesResponse);
        setItemsCount(itemsC);
        setPage(next ? page+1 : page-1)
    }


    return (
        <Layout headTitle="Inicio" pageClass="home-page-cool">
        
        <div className="principal-title" ref={formRef}>
        
            <div className={`title-content-c ${scrollLength > 80 ? 'deactive' : ''}`} style={{width:`calc(${formWidth}px - 2em)`}}>
            
                <h2>Find the most popular games!</h2>

                <p>In Houm you can find a new home and also find a good game to play.</p>

                <div className="form">
                
                    <input type="text" onChange={e => setInput(e.target.value)} onKeyDown={e => e.key === 'Enter' ? searchGame() : ''}/>

                    <button onClick={searchGame}>Search</button>
                
                </div>

                <div className={`vanish-div ${scrollLength > 80 ? 'active' : ''}`}>
            </div>
            
            </div>

            <div className={`circle-cool ${scrollLength > 80 ? 'deactive' : ''}`}>

            </div>

            <div className="little-circle">
 
            </div>

            
        
        </div>

        

        <div className="games-content">

        <div className="paginator-cool left">

        {page > 1 ? <button onClick={() => changePage(false)}>{'<'}</button>:''}
        
        </div>

        <div className="paginator-cool right">

        {page <= (Math.ceil(itemsCount/10)) ? <button onClick={() => changePage(true)}>{'>'}</button>:''}
        
        </div>
        
        {gameData ? <GameList gameData={gameData}/> : <Loading/>}

        <div className="paginator-cool both">

        {page > 1 ? <button onClick={() => changePage(false)}>{'<'}</button>:''}
        {page <= (Math.ceil(itemsCount/10)) ? <button onClick={() => changePage(true)}>{'>'}</button>:''}
        
        </div>
        
        </div>

        </Layout>
    );
};

export default Home;