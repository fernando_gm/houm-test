import './App.css';
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom'
import Home from './components/views/Home';
import Game from './components/views/Game';

function App() {
  return (
    <HashRouter>
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/search' component={Home}/>
        <Route exact path='/juego/:id' component={Game}/>
        <Redirect from='*' to='/' />
      </Switch>
    </HashRouter>
  );
}

export default App;
