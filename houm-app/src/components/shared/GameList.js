import React, { useState, useEffect, useRef, Fragment } from 'react';
import { Link } from 'react-router-dom';
import './css/gamelist.css';
import RatingCalulateStars from './RatingCalulateStars';

const GameList = ({gameData}) => {

    const [gameListWidth, setGameListWidth] = useState(0);
    
    const gameListRef = useRef(0);

    useEffect(() => {
        if(gameListRef.current) setGameListWidth(gameListRef.current.offsetWidth);
    }, [])

    const resizeDiv = () => {
        if(gameListRef.current) setGameListWidth(gameListRef.current.offsetWidth);
    }

    window.addEventListener('resize', resizeDiv);

    return (
        <div className="games-content" ref={gameListRef}>
        
            {
                gameData.map(game => (
                    <div className="game-content-cool" key={game.slug} >
                    
                        <div className="img-content">
            
                            <img src={game.background_image} alt="" style={{height:`${gameListWidth * .30}px`}}/>
            
                            {game.metacritic ? <div className="metascore">{game.metacritic}</div> : ''}
                        
                        </div>
            
                        <div className="content-container-c">
                        
                            <h3><Link to={`/juego/${game.id}`}>{game.name}</Link></h3>

                            <RatingCalulateStars rating={game.rating} />
            
                            <div className="ratings-stats">
                            
                                <div className="stat-c">
                                    <i className="lni lni-heart"></i><p>{game.ratings[0] ? game.ratings[0].count : '--'}</p>
                                </div>
                                <div className="stat-c">
                                    <i className="lni lni-emoji-smile"></i><p>{game.ratings[1] ? game.ratings[1].count : '--'}</p>
                                </div>
                                <div className="stat-c">
                                    <i className="lni lni-emoji-speechless"></i><p>{game.ratings[2] ? game.ratings[2].count : '--'}</p>
                                </div>
                                
                            </div>
            
                        </div>
            
                    </div>
                ))
            }
        
        </div>
    ) 
     
    
};

export default GameList;