import React from 'react';
import './css/loading.css';

const Loading = () => {
    return (
        <div className="loading-c">

            <p>Cargando</p>
            
            <div className="loading-circles">
            
                <div className="circle-loading one"></div>
                <div className="circle-loading two"></div>
                <div className="circle-loading three"></div>
                <div className="circle-loading four"></div>
            
            </div>

        </div>
    );
};

export default Loading;