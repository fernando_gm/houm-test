import React from 'react';
import { Link } from 'react-router-dom';
import './css/navbar.css'

const Navbar = () => {
    return (
        <nav className="navbar-houm-cool">

            <div className="navbar-content-cool">
            
                <a href="https://houm.com" className="logo">
                
                    <img src="/img/logo.png" alt="houm-logo"/>

                </a>
                
                <ul>
                
                    <li><Link to="/">Home</Link></li>
                    <li><a href="https://bitbucket.org/fernando_gm/houm-test/src/master/">Repository</a></li>

                </ul>
            
            </div>

        </nav>
    );
};

export default Navbar;