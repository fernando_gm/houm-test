import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';
import Navbar from './Navbar';
import './css/layout.css'

const Layout = ({children, headTitle, pageClass}) => {
    return (
        <Fragment>
        
            <Helmet>
                <title>{headTitle}</title>
                <meta name="description" content="Houm Game App" data-react-helmet="true"></meta>
                <link rel="preconnect" href="https://fonts.gstatic.com"/>
                <link href="https://fonts.googleapis.com/css2?family=Karla:wght@200;300;400;500&display=swap" rel="stylesheet"/>
                <link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet"/>
            </Helmet>

            <Navbar/>

            <div className={`layout-content-cool ${pageClass}`}>
                {children}
            </div>
        
        </Fragment>
    );
};

export default Layout;