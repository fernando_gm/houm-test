import React, { Fragment, useState, useEffect } from 'react';
import axios from 'axios';
import Layout from '../shared/Layout';
import './css/game.css';
import RatingCalulateStars from '../shared/RatingCalulateStars';
import Loading from '../shared/Loading';

const Game = (props) => {

    const id = props.match.params.id;

    const [gameData, setGameData] = useState(null);

    useEffect(() => {
        getGamesData();
        return () => {
            
        }
    }, [])

    const getGamesData = async () => {
        const res = await axios.get(`https://api.rawg.io/api/games/${id}`, {
            params: {
                key: process.env.REACT_APP_API_KEY
            }
        })

        
        const gamesResponse = res.data ? res.data : null;
        
        setGameData(gamesResponse);

    }

    if (!gameData) return <Loading/>;

    return (
        <Layout headTitle="Inicio" pageClass="game-page-cool">
        
            <div className="img-background-cool">
            
                <img className="background" src={gameData.background_image} alt=""/>

                <div className="glass-cool"></div>
                
                <img className="img-fade-cool" src="/img/bottom-fade2.png"/>

                <h2>{gameData.name}</h2>
                
                <RatingCalulateStars rating={gameData.rating} />

                <div className="ratings-stats">
                            
                    <div className="stat-c">
                        <i className="lni lni-heart"></i><p>{gameData.ratings[0] ? gameData.ratings[0].count : '--'}</p>
                    </div>
                    <div className="stat-c">
                        <i className="lni lni-emoji-smile"></i><p>{gameData.ratings[1] ? gameData.ratings[1].count : '--'}</p>
                    </div>
                    <div className="stat-c">
                        <i className="lni lni-emoji-speechless"></i><p>{gameData.ratings[2] ? gameData.ratings[2].count : '--'}</p>
                    </div>
                    
                </div>

            
            </div>

            

            <div className="page-content-c">
            
            <p className="description-c">{gameData.description_raw}</p>
            
            </div>

        </Layout>
    );
};

export default Game;