# Houm Test

## Requeriments

- NodeJS

## Install Dependencies

- Open a terminal
- You must be located at ```houm-test/houm-app```
- Run the command ```npm install``` or ```npm i```

## Run Project

- Open a terminal
- You must be located at ```houm-test/houm-app```
- Run the command ```npm run start``` or ```npm start```
- Open your browser and go to ```127.0.0.1:3000``` or ```localhost:3000```
