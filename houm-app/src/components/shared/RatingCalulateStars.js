import React, { Fragment } from 'react';
import './css/ratingcalculatestars.css';

const RatingCalulateStars = ({rating}) => {

    const ratingCalculate = (actual) => {

        if (actual > 5) return '';

        let starClass = '';

        if(rating >= actual){
            starClass = '';
        }
        else if (rating > actual - .51) {
            starClass = '-half';
        } else {
            starClass = '-empty';
        }


        return ( 
            <Fragment>
                <i className={`lni lni-star${starClass}`}></i>
                {ratingCalculate(actual + 1)} 
            </Fragment>
            );
    }

    return (
        <div className="star-content">
            {ratingCalculate(1)}
        </div>
    );
};

export default RatingCalulateStars;